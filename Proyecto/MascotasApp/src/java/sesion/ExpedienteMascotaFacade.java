/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.ExpedienteMascota;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author alex
 */
@Stateless
public class ExpedienteMascotaFacade extends AbstractFacade<ExpedienteMascota> {

    @PersistenceContext(unitName = "MascotasAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ExpedienteMascotaFacade() {
        super(ExpedienteMascota.class);
    }
    
}
