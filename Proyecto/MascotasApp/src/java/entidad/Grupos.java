/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "grupos")
@NamedQueries({
    @NamedQuery(name = "Grupos.findAll", query = "SELECT g FROM Grupos g"),
    @NamedQuery(name = "Grupos.findByUsuarioscorreo", query = "SELECT g FROM Grupos g WHERE g.usuarioscorreo = :usuarioscorreo"),
    @NamedQuery(name = "Grupos.findByRol", query = "SELECT g FROM Grupos g WHERE g.rol = :rol")})
public class Grupos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Usuarios_correo")
    private String usuarioscorreo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "rol")
    private String rol;
    @JoinColumn(name = "Usuarios_correo", referencedColumnName = "correo", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Usuarios usuarios;

    public Grupos() {
    }

    public Grupos(String usuarioscorreo) {
        this.usuarioscorreo = usuarioscorreo;
    }

    public Grupos(String usuarioscorreo, String rol) {
        this.usuarioscorreo = usuarioscorreo;
        this.rol = rol;
    }

    public String getUsuarioscorreo() {
        return usuarioscorreo;
    }

    public void setUsuarioscorreo(String usuarioscorreo) {
        this.usuarioscorreo = usuarioscorreo;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioscorreo != null ? usuarioscorreo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupos)) {
            return false;
        }
        Grupos other = (Grupos) object;
        if ((this.usuarioscorreo == null && other.usuarioscorreo != null) || (this.usuarioscorreo != null && !this.usuarioscorreo.equals(other.usuarioscorreo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Grupos[ usuarioscorreo=" + usuarioscorreo + " ]";
    }
    
}
