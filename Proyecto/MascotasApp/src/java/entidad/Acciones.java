/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "acciones")
@NamedQueries({
    @NamedQuery(name = "Acciones.findAll", query = "SELECT a FROM Acciones a"),
    @NamedQuery(name = "Acciones.findByIdaccion", query = "SELECT a FROM Acciones a WHERE a.idaccion = :idaccion"),
    @NamedQuery(name = "Acciones.findByAccion", query = "SELECT a FROM Acciones a WHERE a.accion = :accion"),
    @NamedQuery(name = "Acciones.findByFecha", query = "SELECT a FROM Acciones a WHERE a.fecha = :fecha")})
public class Acciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idaccion")
    private Integer idaccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "accion")
    private Character accion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "Usuarios_correo", referencedColumnName = "correo")
    @ManyToOne(optional = false)
    private Usuarios usuarioscorreo;
    @JoinColumn(name = "expediente_mascota_idexpediente", referencedColumnName = "idexpediente")
    @ManyToOne(optional = false)
    private ExpedienteMascota expedienteMascotaIdexpediente;

    public Acciones() {
    }

    public Acciones(Integer idaccion) {
        this.idaccion = idaccion;
    }

    public Acciones(Integer idaccion, Character accion, Date fecha, String descripcion) {
        this.idaccion = idaccion;
        this.accion = accion;
        this.fecha = fecha;
        this.descripcion = descripcion;
    }

    public Integer getIdaccion() {
        return idaccion;
    }

    public void setIdaccion(Integer idaccion) {
        this.idaccion = idaccion;
    }

    public Character getAccion() {
        return accion;
    }

    public void setAccion(Character accion) {
        this.accion = accion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Usuarios getUsuarioscorreo() {
        return usuarioscorreo;
    }

    public void setUsuarioscorreo(Usuarios usuarioscorreo) {
        this.usuarioscorreo = usuarioscorreo;
    }

    public ExpedienteMascota getExpedienteMascotaIdexpediente() {
        return expedienteMascotaIdexpediente;
    }

    public void setExpedienteMascotaIdexpediente(ExpedienteMascota expedienteMascotaIdexpediente) {
        this.expedienteMascotaIdexpediente = expedienteMascotaIdexpediente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idaccion != null ? idaccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acciones)) {
            return false;
        }
        Acciones other = (Acciones) object;
        if ((this.idaccion == null && other.idaccion != null) || (this.idaccion != null && !this.idaccion.equals(other.idaccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return idaccion+"";
    }
    
}
