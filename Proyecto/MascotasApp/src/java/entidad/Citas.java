/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "citas")
@NamedQueries({
    @NamedQuery(name = "Citas.findAll", query = "SELECT c FROM Citas c"),
    @NamedQuery(name = "Citas.findByIdreservaci\u00f3n", query = "SELECT c FROM Citas c WHERE c.idreservaci\u00f3n = :idreservaci\u00f3n"),
    @NamedQuery(name = "Citas.findByFechaReservacion", query = "SELECT c FROM Citas c WHERE c.fechaReservacion = :fechaReservacion"),
    @NamedQuery(name = "Citas.findByHora", query = "SELECT c FROM Citas c WHERE c.hora = :hora"),
    @NamedQuery(name = "Citas.findByMotivo", query = "SELECT c FROM Citas c WHERE c.motivo = :motivo"),
    @NamedQuery(name = "Citas.findByComentarios", query = "SELECT c FROM Citas c WHERE c.comentarios = :comentarios"),
    @NamedQuery(name = "Citas.findByEstatus", query = "SELECT c FROM Citas c WHERE c.estatus = :estatus")})
public class Citas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idreservaci\u00f3n")
    private Integer idreservación;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_reservacion")
    @Temporal(TemporalType.DATE)
    private Date fechaReservacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "hora")
    private String hora;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "motivo")
    private String motivo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "comentarios")
    private String comentarios;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estatus")
    private Character estatus;
    @JoinColumn(name = "Mascota_idMascota", referencedColumnName = "idMascota")
    @ManyToOne(optional = false)
    private Mascota mascotaidMascota;
    @JoinColumn(name = "usuarios_correo", referencedColumnName = "correo")
    @ManyToOne(optional = false)
    private Usuarios usuariosCorreo;

    public Citas() {
    }

    public Citas(Integer idreservación) {
        this.idreservación = idreservación;
    }

    public Citas(Integer idreservación, Date fechaReservacion, String hora, String motivo, String comentarios, Character estatus) {
        this.idreservación = idreservación;
        this.fechaReservacion = fechaReservacion;
        this.hora = hora;
        this.motivo = motivo;
        this.comentarios = comentarios;
        this.estatus = estatus;
    }

    public Integer getIdreservación() {
        return idreservación;
    }

    public void setIdreservación(Integer idreservación) {
        this.idreservación = idreservación;
    }

    public Date getFechaReservacion() {
        return fechaReservacion;
    }

    public void setFechaReservacion(Date fechaReservacion) {
        this.fechaReservacion = fechaReservacion;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Character getEstatus() {
        return estatus;
    }

    public void setEstatus(Character estatus) {
        this.estatus = estatus;
    }

    public Mascota getMascotaidMascota() {
        return mascotaidMascota;
    }

    public void setMascotaidMascota(Mascota mascotaidMascota) {
        this.mascotaidMascota = mascotaidMascota;
    }

    public Usuarios getUsuariosCorreo() {
        return usuariosCorreo;
    }

    public void setUsuariosCorreo(Usuarios usuariosCorreo) {
        this.usuariosCorreo = usuariosCorreo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idreservación != null ? idreservación.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Citas)) {
            return false;
        }
        Citas other = (Citas) object;
        if ((this.idreservación == null && other.idreservación != null) || (this.idreservación != null && !this.idreservación.equals(other.idreservación))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return idreservación+"";
    }
    
}
