/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "expediente_mascota")
@NamedQueries({
    @NamedQuery(name = "ExpedienteMascota.findAll", query = "SELECT e FROM ExpedienteMascota e"),
    @NamedQuery(name = "ExpedienteMascota.findByIdexpediente", query = "SELECT e FROM ExpedienteMascota e WHERE e.idexpediente = :idexpediente"),
    @NamedQuery(name = "ExpedienteMascota.findByFechaCreacion", query = "SELECT e FROM ExpedienteMascota e WHERE e.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "ExpedienteMascota.findByPeso", query = "SELECT e FROM ExpedienteMascota e WHERE e.peso = :peso"),
    @NamedQuery(name = "ExpedienteMascota.findByOperaciones", query = "SELECT e FROM ExpedienteMascota e WHERE e.operaciones = :operaciones"),
    @NamedQuery(name = "ExpedienteMascota.findByObservacionesGenerales", query = "SELECT e FROM ExpedienteMascota e WHERE e.observacionesGenerales = :observacionesGenerales"),
    @NamedQuery(name = "ExpedienteMascota.findByTama\u00f1o", query = "SELECT e FROM ExpedienteMascota e WHERE e.tama\u00f1o = :tama\u00f1o")})
public class ExpedienteMascota implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idexpediente")
    private Integer idexpediente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "peso")
    private double peso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "operaciones")
    private Character operaciones;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "observaciones_generales")
    private String observacionesGenerales;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tama\u00f1o")
    private int tamaño;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "expedienteMascotaIdexpediente")
    private Collection<Acciones> accionesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "expedienteMascotaIdexpediente")
    private Collection<Mascota> mascotaCollection;

    public ExpedienteMascota() {
    }

    public ExpedienteMascota(Integer idexpediente) {
        this.idexpediente = idexpediente;
    }

    public ExpedienteMascota(Integer idexpediente, Date fechaCreacion, double peso, Character operaciones, String observacionesGenerales, int tamaño) {
        this.idexpediente = idexpediente;
        this.fechaCreacion = fechaCreacion;
        this.peso = peso;
        this.operaciones = operaciones;
        this.observacionesGenerales = observacionesGenerales;
        this.tamaño = tamaño;
    }

    public Integer getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Integer idexpediente) {
        this.idexpediente = idexpediente;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public Character getOperaciones() {
        return operaciones;
    }

    public void setOperaciones(Character operaciones) {
        this.operaciones = operaciones;
    }

    public String getObservacionesGenerales() {
        return observacionesGenerales;
    }

    public void setObservacionesGenerales(String observacionesGenerales) {
        this.observacionesGenerales = observacionesGenerales;
    }

    public int getTamaño() {
        return tamaño;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }

    public Collection<Acciones> getAccionesCollection() {
        return accionesCollection;
    }

    public void setAccionesCollection(Collection<Acciones> accionesCollection) {
        this.accionesCollection = accionesCollection;
    }

    public Collection<Mascota> getMascotaCollection() {
        return mascotaCollection;
    }

    public void setMascotaCollection(Collection<Mascota> mascotaCollection) {
        this.mascotaCollection = mascotaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idexpediente != null ? idexpediente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExpedienteMascota)) {
            return false;
        }
        ExpedienteMascota other = (ExpedienteMascota) object;
        if ((this.idexpediente == null && other.idexpediente != null) || (this.idexpediente != null && !this.idexpediente.equals(other.idexpediente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.ExpedienteMascota[ idexpediente=" + idexpediente + " ]";
    }
    
}
