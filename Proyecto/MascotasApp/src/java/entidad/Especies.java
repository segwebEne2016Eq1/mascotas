/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "especies")
@NamedQueries({
    @NamedQuery(name = "Especies.findAll", query = "SELECT e FROM Especies e"),
    @NamedQuery(name = "Especies.findByIdEspecie", query = "SELECT e FROM Especies e WHERE e.idEspecie = :idEspecie"),
    @NamedQuery(name = "Especies.findByNombreEspecie", query = "SELECT e FROM Especies e WHERE e.nombreEspecie = :nombreEspecie"),
    @NamedQuery(name = "Especies.findByActivoEspecie", query = "SELECT e FROM Especies e WHERE e.activoEspecie = :activoEspecie")})
public class Especies implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEspecie")
    private Integer idEspecie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre_especie")
    private String nombreEspecie;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo_especie")
    private Character activoEspecie;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "especiesidEspecie")
    private Collection<Mascota> mascotaCollection;

    public Especies() {
    }

    public Especies(Integer idEspecie) {
        this.idEspecie = idEspecie;
    }

    public Especies(Integer idEspecie, String nombreEspecie, Character activoEspecie) {
        this.idEspecie = idEspecie;
        this.nombreEspecie = nombreEspecie;
        this.activoEspecie = activoEspecie;
    }

    public Integer getIdEspecie() {
        return idEspecie;
    }

    public void setIdEspecie(Integer idEspecie) {
        this.idEspecie = idEspecie;
    }

    public String getNombreEspecie() {
        return nombreEspecie;
    }

    public void setNombreEspecie(String nombreEspecie) {
        this.nombreEspecie = nombreEspecie;
    }

    public Character getActivoEspecie() {
        return activoEspecie;
    }

    public void setActivoEspecie(Character activoEspecie) {
        this.activoEspecie = activoEspecie;
    }

    public Collection<Mascota> getMascotaCollection() {
        return mascotaCollection;
    }

    public void setMascotaCollection(Collection<Mascota> mascotaCollection) {
        this.mascotaCollection = mascotaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEspecie != null ? idEspecie.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Especies)) {
            return false;
        }
        Especies other = (Especies) object;
        if ((this.idEspecie == null && other.idEspecie != null) || (this.idEspecie != null && !this.idEspecie.equals(other.idEspecie))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombreEspecie;
    }
    
}
