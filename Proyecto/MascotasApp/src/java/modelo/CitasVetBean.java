/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Luis
 */
@Named(value = "citasVet")
@SessionScoped
public class CitasVetBean implements Serializable {

    
    private String nombre;
    private List<String[]> citasPendientes;
    private Date searchDate;
    
    /**
     * Creates a new instance of CitasVetBean
     */
    public CitasVetBean() {
        
        this.nombre = "Rogelio Ruiz";
        this.citasPendientes = citasFactory();
        this.searchDate = new Date();
    }
    
    private List<String[]> citasFactory(){
        List<String[]> citas = new ArrayList();
        citas.add(new String[]{"26/05/2015","9:00 AM", "Penny","Luis Arriaga","Pendiente"});
        citas.add(new String[]{"26/05/2015","10:00 AM", "Manchas","Roberto Gómez","Pendiente"});
        citas.add(new String[]{"26/05/2015","12:00 PM", "Rex","Karla López","Pendiente"});
        citas.add(new String[]{"26/05/2015","15:00 PM", "Bestia","Rodolfo Arnulfo","Pendiente"});
        citas.add(new String[]{"26/05/2015","17:00 PM", "Dama","Paola Zárate","Pendiente"});
        citas.add(new String[]{"26/05/2015","18:00 PM", "Bigotes","Verónica Olmos","Pendiente"});
        return citas;
    }

    public String getNombre() {
        return "<b>Dr. "+nombre+"</b>";
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<String[]> getCitasPendientes() {
        return citasPendientes;
    }

    public void setCitasPendientes(List<String[]> citasPendientes) {
        this.citasPendientes = citasPendientes;
    }

    public int getTotalCitas(){
        return this.citasPendientes.size();
                
    }

    public Date getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(Date searchDate) {
        this.searchDate = searchDate;
    }
    
    public Date getMinFecha(){
        return new Date();
    }
       
}
