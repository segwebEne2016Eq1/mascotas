/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Luis
 */
@Named(value = "agendaCita")
@SessionScoped
public class AgendaCitaBean implements Serializable {

    
    private Date fechaBusqueda;
    private String horaCita;
    private List<String[]> horasDia;
    
    //Atributos del detalle.
    private int mascota;
    private char motivo;
    private int veterinario;
    private String observaciones;
    
    //Atributos de control interno.
    private int horaActual;
    
    /**
     * Creates a new instance of AgendaCitaBean
     */
    public AgendaCitaBean() {
        
        this.fechaBusqueda = new Date();
//        Calendar cal = Calendar.getInstance();
//        this.horaActual=cal.get(Calendar.HOUR_OF_DAY);
    }
    
    private List<String[]> horasFactory(){
        List<String[]> retorno = new ArrayList();
        for(int i=9; i<=18; i++){
            if(i <= 13 || i >= 15){
                String horaLabel = i+":00 ";
                horaLabel+=(i<12)?"AM":"PM";
                
                double prob = Math.random();
                
                
                String estado = (prob > 0.5)?"Disponible":"Ocupada";
                String clase =(prob > 0.5)?"hora-libre":"hora-ocupada";
                
                
                String[] hora = new String[]{horaLabel,estado,clase};
                retorno.add(hora);
                
            }            
        }
        return retorno;
    }

    public Date getFechaBusqueda() {
        return fechaBusqueda;
    }
    
    public Date getMinFecha(){
        return new Date();
    }

    public void setFechaBusqueda(Date fechaBusqueda) {
        this.fechaBusqueda = fechaBusqueda;
    }

    public List<String[]> getHorasDia() {
        this.horasDia = horasFactory();
        return horasDia;
    }

    public void setHorasDia(List<String[]> horasDia) {
        this.horasDia = horasDia;
    }

    public String getHoraCita() {
        return horaCita;
    }

    public void setHoraCita(String horaCita) {
        this.horaCita = horaCita;
    }

    public int getMascota() {
        return mascota;
    }

    public void setMascota(int mascota) {
        this.mascota = mascota;
    }

    public char getMotivo() {
        return motivo;
    }

    public void setMotivo(char motivo) {
        this.motivo = motivo;
    }

    public int getVeterinario() {
        return veterinario;
    }

    public void setVeterinario(int veterinario) {
        this.veterinario = veterinario;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    
    
    
    
    
    
}
