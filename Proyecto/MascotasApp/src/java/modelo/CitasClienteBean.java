/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Luis
 */
@Named(value = "citasCliente")
@SessionScoped
public class CitasClienteBean implements Serializable {

    /**
     * Creates a new instance of CitasClienteBean
     */
    public CitasClienteBean() {
        
        this.nombre="Luis Arriaga";
        this.citasPendientes = citasFactory();
    }
    
    private String nombre;
    private List<String[]> citasPendientes;
    
    private List<String[]> citasFactory(){
        List<String[]> citas = new ArrayList();
        citas.add(new String[]{"26/05/2016","10:00 AM","Manchas","Pendiente"});
        citas.add(new String[]{"30/05/2016","09:00 AM","Manchas","Pendiente"});
        citas.add(new String[]{"02/06/2016","12:00 PM","Bigotes","Pendiente"});
        citas.add(new String[]{"15/06/2016","10:00 AM","Manchas","Pendiente"});
        citas.add(new String[]{"01/07/2016","18:00 PM","Bigotes","Pendiente"});
        citas.add(new String[]{"16/07/2016","15:00 PM","Manchas","Pendiente"});
        citas.add(new String[]{"03/08/2016","17:00 PM","Bigotes","Pendiente"});
        citas.add(new String[]{"20/08/2016","10:00 AM","Manchas","Pendiente"});
        return citas;
    }

    public String getNombre() {
        return "<b>"+nombre+"</b>";
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<String[]> getCitasPendientes() {
        return citasPendientes;
    }

    public void setCitasPendientes(List<String[]> citasPendientes) {
        this.citasPendientes = citasPendientes;
    }
    
    public int getTotalCitas(){
        return this.citasPendientes.size();
    }
    
    
    
}
